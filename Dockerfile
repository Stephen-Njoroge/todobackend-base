FROM ubuntu:trusty
MAINTAINER Stephen Njoroge <steven.n360@gmail.com>

# Prevent dpkg errors
ENV TERM=xterm-256color

#install python and virtualenv
Run apt-get update && \
    apt-get install -y \
    -o APT::Install-Recommend=false -o APT::Install-Suggesst=false \
    python python-virtualenv libpython2.7 python-mysqldb

# create virtualenv and upgrade pip
RUN virtualenv /appenv && \
    . /appenv/bin/activate && \
    pip install pip --upgrade

# Add entrypoint script
ADD scripts/entrypoint.sh /usr/local/bin/entrypoint.sh
RUN chmod +x /usr/local/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]

LABEL application=todobackend
